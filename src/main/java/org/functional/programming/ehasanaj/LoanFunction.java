package org.functional.programming.ehasanaj;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.functional.programming.ehasanaj.model.Order;
import org.functional.programming.ehasanaj.service.OrderService;
import org.jooq.lambda.Unchecked;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

//@Component
public class LoanFunction implements CommandLineRunner {

	@Override
	public void run(String... args) throws Exception {
		FileExporter fileExporter = new FileExporter();
		OrderExportWriter orderExportWriter = new OrderExportWriter();
		UserExportWriter userExportWriter = new UserExportWriter();
		
		fileExporter.exportFile("test1.csv", Unchecked.consumer(orderExportWriter::writeContent));
		fileExporter.exportFile("test2.csv", Unchecked.consumer(userExportWriter::writeContent));
	}
}

class FileExporter {

	private OrderService orderService = new OrderService();
	private static final Logger log = LoggerFactory.getLogger(FileExporter.class);

	public File exportFile(String fileName, Consumer<Writer> contentWriter) throws Exception {
		File file = new File("/Users/ehasanaj/Desktop/" + fileName);
		try (Writer writer = new FileWriter(file)) {
			contentWriter.accept(writer);
			return file;
		} catch (Exception e) {
			log.debug("Gotcha!", e);
			throw e;
		}
	}
}

class OrderExportWriter {
	private OrderService orderService = new OrderService();
	
	protected void writeContent(Writer writer) throws IOException {
		writer.write("OrderId;Date\n");
		orderService.findByActiveTrue()
				.map(o -> o.getId() + ";" + o.getCreationDate() + "\n")
				.forEach(Unchecked.consumer(writer::write));
	}
}

class UserExportWriter {
	private OrderService orderService = new OrderService();
	
	protected void writeContent(Writer writer) throws IOException {
		writer.write("UserId;UserName\n");
		writer.write("24;ehasanaj");
	}
}