package org.functional.programming.ehasanaj;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;

import org.functional.programming.ehasanaj.model.Order;
import org.functional.programming.ehasanaj.model.Product;
import org.functional.programming.ehasanaj.repositorie.ProductRepo;
import org.functional.programming.ehasanaj.service.OrderService;
import org.functional.programming.ehasanaj.utilities.OrderLine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

//@Component
public class StreamWreck implements CommandLineRunner{
	
	@Autowired
	ProductRepo productRepo;
	
	@Autowired
	OrderService orderService;

	@Override
	public void run(String... args) throws Exception {
		System.out.println("HIHI works");
		
		List<Order> orders =  orderService.getOrders();
		orders.stream().forEach(System.out::println);
		
		List<Product> frequentOrderedProiducts = getFrequentOrderedProiducts(orders);
		System.out.println(frequentOrderedProiducts.size());
	}
	

	public List<Product> getFrequentOrderedProiducts(List<Order> orders){
		List<Integer> hiddenProductIds = productRepo.getHiddenProductId();
		Predicate<Product> productIsNotHidden = p -> !hiddenProductIds.contains(p.getId());
		Stream<Product> frequentProduct = getProductCountOverThePreviousYear(orders).entrySet().stream()
				.filter(this::validateFrequentProduct)
				.map(Entry::getKey);
		return frequentProduct
				.filter(Product::isNotDeleted)
				.filter(productIsNotHidden)
				.collect(toList());
	}


	private boolean validateFrequentProduct(Entry<Product, Integer> e) {
		System.out.println("-> " + e.getValue());
		return e.getValue() >= 37;
	}

	private Map<Product, Integer> getProductCountOverThePreviousYear(List<Order> orders) {
		Predicate<Order> hasValidCreationDate = o -> o.getCreationDate().isAfter(LocalDate.now().minusYears(1));
		return orders.stream()
				.filter(hasValidCreationDate)
				.flatMap(o -> o.getOrderLines().stream())
				.collect(groupingBy(OrderLine::getProduct, summingInt(OrderLine::getItemCount)));
	}
}
