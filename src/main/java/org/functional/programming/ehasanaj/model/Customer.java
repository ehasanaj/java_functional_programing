package org.functional.programming.ehasanaj.model;

import static java.util.Optional.ofNullable;

import java.util.Optional;

public class Customer {
	private MemberCard memberCard;

	public Customer(MemberCard card) {
		this.memberCard = card;
	}

	public Customer() {	}

	public Optional<MemberCard> getMemberCard() {
		return ofNullable(memberCard);
	}

	public void setMemberCard(MemberCard memberCard) {
		this.memberCard = memberCard;
	}
}
