package org.functional.programming.ehasanaj.model;

import java.util.Date;

public class User {
	private String username;
	private String firstName;
	private String lastName;
	private Date activationDate;
	private Date deasctivationDate;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getActivationDate() {
		return activationDate;
	}
	public void setActivationDate(Date activationDate) {
		this.activationDate = activationDate;
	}
	public Date getDeasctivationDate() {
		return deasctivationDate;
	}
	public void setDeasctivationDate(Date deasctivationDate) {
		this.deasctivationDate = deasctivationDate;
	}
}
