package org.functional.programming.ehasanaj.model;

import java.time.LocalDate;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.functional.programming.ehasanaj.utilities.OrderLine;

public class Order {
	
	private Integer id;
	
	private List<OrderLine> orderLine;

	public LocalDate getCreationDate() {
		Random rand = new Random();
		return LocalDate.now().minusDays(rand.nextInt(50));
	}

	public List<OrderLine> getOrderLines() {
		return orderLine;
	}

	public void setOrderLine(List<OrderLine> orderLine) {
		this.orderLine = orderLine;
	}

	public Integer getId() {
		Random rand = new Random();
		return rand.nextInt(50);
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return orderLine.stream()
				.map(line -> line.getProduct().getId().toString() + ":")
				.collect(Collectors.joining(":"));
	}
}
