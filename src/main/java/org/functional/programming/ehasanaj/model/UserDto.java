package org.functional.programming.ehasanaj.model;

public class UserDto {
	private String username;
	private String fullName;
	private Boolean active;
	public UserDto() {}
	public UserDto(User user) {
		this.setUsername(user.getUsername());
 		this.setFullName(user.getFirstName() + " " + user.getLastName());
		this.setActive(user.getDeasctivationDate() == null);
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
}
