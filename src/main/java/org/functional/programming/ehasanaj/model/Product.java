package org.functional.programming.ehasanaj.model;

public class Product {
	private boolean isDeleted;
	private Integer id;
	
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public Integer getId() {
		return this.id;
	}

	public boolean isNotDeleted() {
		return !isDeleted;
	}

}
