package org.functional.programming.ehasanaj.model;

public class MemberCard {
	private Integer fidelityPoints;

	public MemberCard(int fidelityPoints) {
		this.fidelityPoints = fidelityPoints;
	}

	public Integer getFidelityPoints() {
		return fidelityPoints;
	}

	public void setFidelityPoints(Integer fidelityPoints) {
		this.fidelityPoints = fidelityPoints;
	}
}
