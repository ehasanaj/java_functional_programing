package org.functional.programming.ehasanaj.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.functional.programming.ehasanaj.model.Order;
import org.functional.programming.ehasanaj.model.Product;
import org.functional.programming.ehasanaj.utilities.OrderLine;
import org.springframework.stereotype.Service;

@Service
public class OrderService {
	
	public List<Order> getOrders() {
		List<OrderLine> orderLineList = new ArrayList<>(); 
		
		OrderLine orderLine = new OrderLine();
		orderLine.setItemCount(6);
		OrderLine orderLine1 = new OrderLine();
		orderLine1.setItemCount(12);
		OrderLine orderLine2 = new OrderLine();
		orderLine2.setItemCount(50);
		
		Product product = getProduct(1, false);
		Product product1 = getProduct(2, true);
		Product product2 = getProduct(3, false);
		
		orderLine.setProduct(product);
		orderLine1.setProduct(product1);
		orderLine2.setProduct(product2);
		
		orderLineList.add(orderLine);
		orderLineList.add(orderLine1);
		orderLineList.add(orderLine2);
		
		Order order1 = new Order();
		order1.setOrderLine(orderLineList);
		
		Order order2 = new Order();
		order2.setOrderLine(orderLineList);

		Order order3 = new Order();
		order3.setOrderLine(orderLineList);
	    
		List<Order> orders = new ArrayList<>();
		orders.add(order1);
		orders.add(order2);
		orders.add(order3);
		return orders;
	}
	
	public Stream<Order> findByActiveTrue() {
		List<OrderLine> orderLineList = new ArrayList<>(); 
		
		OrderLine orderLine = new OrderLine();
		orderLine.setItemCount(6);
		OrderLine orderLine1 = new OrderLine();
		orderLine1.setItemCount(12);
		OrderLine orderLine2 = new OrderLine();
		orderLine2.setItemCount(50);
		
		Product product = getProduct(1, false);
		Product product1 = getProduct(2, false);
		Product product2 = getProduct(3, false);
		
		orderLine.setProduct(product);
		orderLine1.setProduct(product1);
		orderLine2.setProduct(product2);
		
		orderLineList.add(orderLine);
		orderLineList.add(orderLine1);
		orderLineList.add(orderLine2);
		
		Order order1 = new Order();
		order1.setOrderLine(orderLineList);
		
		Order order2 = new Order();
		order2.setOrderLine(orderLineList);

		Order order3 = new Order();
		order3.setOrderLine(orderLineList);
	    
		List<Order> orders = new ArrayList<>();
		orders.add(order1);
		orders.add(order2);
		orders.add(order3);
		return orders.stream();
	}

	private Product getProduct(Integer id, boolean isDeleted) {
		Product product = new Product();
		product.setId(id);
		product.setDeleted(isDeleted);
		return product;
	}
}
