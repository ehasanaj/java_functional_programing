package org.functional.programming.ehasanaj;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.function.BiFunction;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class TypeSpecificFunctinality implements CommandLineRunner {

	@Override
	public void run(String... args) throws Exception {
		// mock new release price repository
		NewReleasePriceRepo newReleasePriceRepo = mock(NewReleasePriceRepo.class);
		when(newReleasePriceRepo.getFactor()).thenReturn(2d);
		
		// mock regular price repository
		RegularPriceRepo regularPriceRepo = mock(RegularPriceRepo.class);
		when(regularPriceRepo.getFactor()).thenReturn(1d);
		
		// mock children price repository
		ChildrenPriceRepo childrenPriceRepo = mock(ChildrenPriceRepo.class);
		when(childrenPriceRepo.getFactor()).thenReturn(5d);
		
		PriceService priceService = new PriceService(newReleasePriceRepo, regularPriceRepo, childrenPriceRepo);
		System.out.println(priceService.computePrice(Movie.Type.REGULAR, 2));
		System.out.println(priceService.computePrice(Movie.Type.NEW_RELEASE, 2));
		System.out.println(priceService.computePrice(Movie.Type.CHILDREN, 2));
		System.out.println("COMMIT NOW!");
	}

}

class PriceService {
	
	private NewReleasePriceRepo newReleaseRepo;
	private RegularPriceRepo regularPriceRepo;
	private ChildrenPriceRepo childrenPriceRepo;
	
	public PriceService(NewReleasePriceRepo newReleaseRepo, RegularPriceRepo regularPriceRepo,
			ChildrenPriceRepo childrenPriceRepo) {
		super();
		this.newReleaseRepo = newReleaseRepo;
		this.regularPriceRepo = regularPriceRepo;
		this.childrenPriceRepo = childrenPriceRepo;
	}

	int computeNewReleasePrice(int days) {
		return (int) (days * newReleaseRepo.getFactor());
	}
	
	int computeRegularPrice(int days) {
		return (int) (days + regularPriceRepo.getFactor());
	}
	
	int computeChildrePrice(int days) {
		return (int) childrenPriceRepo.getFactor();
	}
	
	public int computePrice(Movie.Type type, int days) {
		return type.priceAlgo.apply(this, days);
	}
}

class Movie {
	enum Type {
		REGULAR(PriceService::computeRegularPrice), 
		NEW_RELEASE(PriceService::computeNewReleasePrice), 
		CHILDREN(PriceService::computeChildrePrice);
		public final BiFunction<PriceService, Integer, Integer> priceAlgo;

		private Type(BiFunction<PriceService, Integer, Integer> priceAlgo) {
			this.priceAlgo = priceAlgo;
		}
	}

	private final Type type;

	public Movie(Type type) {
		this.type = type;
	}
}

interface NewReleasePriceRepo {
	double getFactor();
}

interface RegularPriceRepo {
	double getFactor();
}

interface ChildrenPriceRepo {
	double getFactor();
}