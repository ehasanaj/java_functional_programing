package org.functional.programming.ehasanaj;

import java.util.List;

import org.functional.programming.ehasanaj.model.UserDto;
import org.functional.programming.ehasanaj.repositorie.UserRepo;
import org.functional.programming.ehasanaj.utilities.UserExtrator;
import org.functional.programming.ehasanaj.utilities.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import static java.util.stream.Collectors.toList;

//@Component
public class Tester implements CommandLineRunner {

	@Autowired
	UserRepo userRepo;
	
	@Autowired
	UserMapper mapper;
	
	@Autowired
	UserExtrator extractor;

	public List<UserDto> getAllUsers() {
		List<UserDto> userList = userRepo.findAll().stream().map(mapper::toDto).collect(toList());
		return userList;
	}

	@Override
	public void run(String... args) throws Exception {
		printValues();
	}

	private void printValues() {
		List<String> fullNames = getAllUsers().stream().map(extractor::userFullName).collect(toList());	
		fullNames.stream().forEach(System.out::println);
	}

}
