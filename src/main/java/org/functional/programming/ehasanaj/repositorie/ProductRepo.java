package org.functional.programming.ehasanaj.repositorie;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class ProductRepo {

	public List<Integer> getHiddenProductId() {
		List<Integer> hiddenProductList = new ArrayList<>();
		hiddenProductList.add(5);
		return hiddenProductList;
	}
}
