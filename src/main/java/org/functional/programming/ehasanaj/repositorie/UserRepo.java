package org.functional.programming.ehasanaj.repositorie;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.functional.programming.ehasanaj.model.User;
import org.springframework.stereotype.Service;

@Service
public class UserRepo {
	public List<User> findAll(){
		List<User> users = new ArrayList<User>();
		
		User user = new User();
		user.setUsername("ehasanaj");
		user.setFirstName("Eraldo");
		user.setLastName("Hasanaj");
		user.setActivationDate(new Date());
		users.add(user);

		User user2 = new User();
		user2.setUsername("jjesku");
		user2.setFirstName("Jonida");
		user2.setLastName("Jesku");
		user2.setActivationDate(new Date());
		users.add(user2);

		return users;
	}
}
