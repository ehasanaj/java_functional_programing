package org.functional.programming.ehasanaj.utilities;

import org.functional.programming.ehasanaj.model.UserDto;
import org.springframework.stereotype.Component;

@Component
public class UserExtrator {
	public String userFullName(UserDto user) {
		return user.getFullName();
	}
}
