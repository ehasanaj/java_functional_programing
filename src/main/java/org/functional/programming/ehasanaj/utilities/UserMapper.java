package org.functional.programming.ehasanaj.utilities;

import org.functional.programming.ehasanaj.model.User;
import org.functional.programming.ehasanaj.model.UserDto;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
	
	public UserDto toDto(User user) {
		UserDto dto = new UserDto();
		dto.setUsername(user.getUsername());
		dto.setFullName(user.getFirstName() + " " + user.getLastName());
		dto.setActive(user.getDeasctivationDate() == null);
		return dto;
	}
	
}
