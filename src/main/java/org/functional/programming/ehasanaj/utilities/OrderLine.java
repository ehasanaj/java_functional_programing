package org.functional.programming.ehasanaj.utilities;

import org.functional.programming.ehasanaj.model.Product;

public class OrderLine {
	private Product product;
	private int itemCount;
	
	public void setItemCount(int itemCount) {
		this.itemCount = itemCount;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Product getProduct(){
		return this.product;
	}
	
	public int getItemCount() {
		return this.itemCount;
	}
}
