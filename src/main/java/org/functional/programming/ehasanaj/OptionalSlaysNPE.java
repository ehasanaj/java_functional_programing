package org.functional.programming.ehasanaj;

import static java.util.Optional.empty;
import static java.util.Optional.of;

import java.util.Optional;

import org.functional.programming.ehasanaj.model.Customer;
import org.functional.programming.ehasanaj.model.MemberCard;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

//@Component
public class OptionalSlaysNPE implements CommandLineRunner {

	@Override
	public void run(String... args) throws Exception {
		// test for 60, 10, no member card
		DiscountService discountService = new DiscountService();
		System.out.println(discountService.getDiscountLine(new Customer(new MemberCard(60))));
		System.out.println(discountService.getDiscountLine(new Customer(new MemberCard(10))));
		System.out.println(discountService.getDiscountLine(new Customer()));
	}
}

class DiscountService {
	public String getDiscountLine(Customer customer) {
		return customer.getMemberCard()
				.flatMap(card -> getApplicableDiscountPercentage(card))
				.map(d -> "Discount: " + d).orElse("");
	}
	
	private Optional<Integer> getApplicableDiscountPercentage(MemberCard card) {
		if(card == null) {
			return empty();
		}
		if(card.getFidelityPoints() >= 100) {
			return of(5);
		}
		if(card.getFidelityPoints() >= 50) {
			return of(3);
		}
		return empty();
	}
}
